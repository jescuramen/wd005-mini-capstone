// WD005 - Mini Capstone

// const passwordOne = document.getElementById('inputPassword1').value;
// const passwordTwo = document.getElementById('inputPassword2').value;
// const userName = document.getElementById('userName').value;
// const emailInput = document.getElementById('inputEmail').value;
// const fieldList = document.getElementsByClassName("form-control");
// const fieldArray = [...fieldList];
//
// const firstName = document.getElementById('firstName').value;
// const lastName = document.getElementById('lastName').value;
// const homeAddress = document.getElementById('homeAddress').value;


// username should be greater than or equal to 10 characters
//create a function to validate the username
function checkUserName(){
	let name = document.getElementById('userName').value;
	if(name.length >= 10){
		return true;
	}else{
		//alert('Username is less than 10 characters')
		document.querySelector('#userName').nextElementSibling.innerHTML = 'Username is less than 10 characters'
		return false;
	}
}

// password must be at least 8 characters
function checkPasswordOne(){
	let pw = document.getElementById('inputPassword1').value;
	if(pw.length > 7){
		return true;
	}else{
		//alert('Password must be at least 8 characters')
		document.querySelector('#inputPassword1').nextElementSibling.innerHTML = 'Password must be at least 8 characters'
		return false;
	}
}

function checkPasswordTwo(){
	let pw = document.getElementById('inputPassword2').value;
	if(pw.length > 7){
		return true;
	}else{
		//alert('Password must be at least 8 characters')
		document.querySelector('#inputPassword2').nextElementSibling.innerHTML = 'Password must be at least 8 characters'
		return false;
	}
}

// email should include the @ symbol
function checkEmail(){
	let email = document.getElementById('inputEmail').value;
	if (email.indexOf('@') > -1){
	  return true;
	}else{
		//alert("Invalid email entered.");
		document.querySelector('#inputEmail').nextElementSibling.innerHTML = 'Invalid email entered.'
		return false;
	}
}

// password 1 and 2 must match
function verifyPassword(){
	const pw1 = document.getElementById('inputPassword1').value;
	const pw2 = document.getElementById('inputPassword2').value;
	if(pw1 === pw2){
		return true;
	}else{
		const passwords = document.getElementsByClassName('password');
		const passwordArray = [...passwords];
		passwordArray.forEach(function(pw){
			pw.nextElementSibling.innerHTML = "Passwords are not the same.";
		})
		return false;
	}
}

// no input elements can be empty
function checkFormEmpty(){
  // get all the inputs within the submitted form
  let inputs = document.getElementsByTagName('input');
  let inputsArray = [...inputs];
	inputsArray.forEach(function(input){
		if(input.value == ""){
			input.nextElementSibling.innerHTML = "This is a required field."
		}
	})
}

//create a reset notif for msg "This is a required field."
function resetNotif(){
	let inputs = document.getElementsByTagName('input');
  let inputsArray = [...inputs];
	inputsArray.forEach(function(input){
		if(input.nextElementSibling.innerHTML === "This is a required field." ||
			input.nextElementSibling.innerHTML === "Username is less than 10 characters" ||
			input.nextElementSibling.innerHTML === "Password must be at least 8 characters" ||
			input.nextElementSibling.innerHTML === "Invalid email entered." ||
			input.nextElementSibling.innerHTML === "Passwords are not the same."){
			//reset the notification to an empty string
			input.nextElementSibling.innerHTML = "";
		}
	})
}

function checkFormFill(){
	let inputs = document.getElementsByTagName('input');
  let inputsArray = [...inputs];
	let allFilled = inputsArray.every(function(input){
		return (input.value != "");
	})
	return allFilled;
}

//the submit button
const submitBtn = document.querySelector('form button');

submitBtn.addEventListener("click", function(){
	//reset notification
	resetNotif();
	//check if form is all filled
	checkFormFill();
	if(checkFormFill()){
		//if true check the user name validation
		checkUserName();
		checkPasswordOne();
		checkPasswordTwo();
		verifyPassword();
		checkEmail();
		if(checkUserName() && checkPasswordOne() && checkPasswordTwo() && verifyPassword() && checkEmail()){
			alert(`Congratulations ${document.getElementById('firstName')
						.value} ${document.getElementById('lastName').value} you are now registered.`)
		}
	}else{//if false
		//run the check form if empty
		checkFormEmpty();
	}
})
